from PQDict import PQDict

class MinMaxHeap:
    def __init__(self):
        self.heap=[]
        self.median=0
        self.minHeap=PQDict()
        self.minHeapSize=0
        self.maxHeap=self.minHeap.maxpq()
        self.maxHeapSize=0
    
    def printmmpq(self):
        print self.minHeapSize,self.minHeap.keys(),self.minHeap.values()
        print self.maxHeapSize,self.maxHeap.keys(),self.maxHeap.values()
        
    def _swap(self):
        #sink
        tmp=self.minHeap.popitem()
        self.maxHeap[tmp[0]]=tmp[1]
        #float
        tmp=self.maxHeap.popitem()
        self.minHeap[tmp[0]]=tmp[1]
        
    
    def _balanceHeap(self):
        if (self.minHeapSize>0) and (self.maxHeapSize>0):
            while self.minHeap.peek()[1] < self.maxHeap.peek()[1]:
                self._swap()
        while (self.maxHeapSize - self.minHeapSize) > 1:                
            tmp=self.maxHeap.popitem()
            self.maxHeapSize-=1
            self.minHeap[tmp[0]]=tmp[1]
            self.minHeapSize+=1
        while self.minHeapSize > self.maxHeapSize:
            tmp=self.minHeap.popitem()
            self.minHeapSize-=1
            self.maxHeap[tmp[0]]=tmp[1]
            self.maxHeapSize+=1
    
    def addNode(self, node):
        self.maxHeap[node]=1
        self.maxHeapSize+=1
        self._balanceHeap()
    
    def delNode(self, node):
        if node in self.minHeap:
            self.minHeap.pop(node)
            self.minHeapSize-=1
        elif node in self.maxHeap:
            self.maxHeap.pop(node)
            self.maxHeapSize-=1
        self._balanceHeap()
    
    def updateNode(self, node, flag):
        if flag==1:
            if node in self.minHeap:
                self.minHeap[node]+=flag
            elif node in self.maxHeap:
                self.maxHeap[node]+=flag
        elif flag==-1:
            if node in self.minHeap:
                self.minHeap[node]+=flag
            elif node in self.maxHeap:
                self.maxHeap[node]+=flag
        self._balanceHeap()
        
    def getMedian(self):
        if self.minHeapSize==self.maxHeapSize:
            return .5*(self.minHeap.peek()[1] + self.maxHeap.peek()[1])
        else:
            return self.maxHeap.peek()[1]*1.0

if __name__ == '__main__':            
	h=list('abbbcdebbbccced')
	pq = MinMaxHeap()
	for i in h:
		if pq.minHeap.get(i) or pq.maxHeap.get(i):
			print i
			pq.updateNode(i,1)
		else:
			pq.addNode(i)
	pq.printmmpq()
	pq.getMedian()