import json #used by parseJsonLine
from dateutil import parser #by parseTimeStamp
from MinMaxHeap import MinMaxHeap
import heapq #used in Graph
import itertools #used in Graph
import sys #used to pass system arg's to the python function

def parseTimeStamp(timestamp):
    #get the relative time since the epoch, Jan 1 00:00:00 UTC 1970
    #get it in seconds in integer format
    return int((parser.parse(timestamp)-parser.parse('1970 Jan 01 00:00:00 +0000')).total_seconds())

def parseJsonLine(line):
    #input: Json line
    #output: (timeTag, wordBag)
    lineJson=json.loads(line)
    if 'created_time' in lineJson.keys():
        if 'actor' in lineJson.keys() and 'target' in lineJson.keys():      
            edge=(lineJson['actor'], lineJson['target'])
            timeTag=parseTimeStamp(lineJson['created_time'])
            return timeTag, edge
        else: #no 'actor' or 'target'
            return
    else: #no 'created_at'
        return
        
class Graph:
    def __init__(self):
        self.graph={}
        self.numberOfEdges=0
        self.numberOfNodes=0
        self.median=0
        self.heap=MinMaxHeap()  
    
    def permuteVertices(func):
        def permute(self, vertex):
            func(self, vertex[0], vertex[1])
            func(self, vertex[1], vertex[0])
        return permute
    
    @permuteVertices
    def addNode(self, A, B):
        if A in self.graph:
            if B in self.graph[A]:
                self.graph[A][B]+=1
            else:
                self.graph[A][B]=1
                self.heap.updateNode(A,1)
                self.numberOfEdges+=1
        else:
            tmp={}
            tmp[B]=1
            self.graph[A]=tmp
            self.numberOfEdges+=1
            self.numberOfNodes+=1
            self.heap.addNode(A)
    
    @permuteVertices
    def removeNode(self, A, B):
        if self.graph[A][B]>1:
            self.graph[A][B]-=1
        else:
            self.graph[A].pop(B, None)
            self.numberOfEdges-=1
            self.heap.updateNode(A,-1)
            if not self.graph[A]:
                del self.graph[A]
                self.numberOfNodes-=1
                self.heap.delNode(A)
    
    def heapPush(self, parsedLine):
        #define heap
        heapq.heappush(self.heap, parsedLine)
    
    def heapPop(self):
        return heapq.heappop(self.heap)
    
    def getMedianDegree(self):
        return self.heap.getMedian()

class RunningWindow:
    def __init__(self, timeWindowInSeconds=60): #default time window span is 60s
        self.timeWindow=timeWindowInSeconds
        self.medianDegree=0.0
        self.graph=Graph()
        self.latestTime=0
        self.heap=[]
        
    def getMedianDegree(self):
    	return self.medianDegree
    
    def processLine(self, line):
        parsedLine=parseJsonLine(line)
        self.processParsedLine(parsedLine)
    
    def processParsedLine(self, parsedLine):
        if parsedLine is not None:
            self.processNonEmptyParsedLine(parsedLine)
    
    def processNonEmptyParsedLine(self, parsedLine):
        #parsedLine = (timeTag, wordBag)
        #timeTag is an integer
        #wordBag is a set of strings
        if (self.latestTime-parsedLine[0]) < self.timeWindow:
            self.updateHeapAndGraph(parsedLine)
            if self.latestTime < parsedLine[0]:
                self.updateLatestTime(parsedLine[0])
                self.balanceHeapAndGraph()
            #print len(self.heap)
            self.updateMedianDegree()
    
    def updateMedianDegree(self):
        self.medianDegree=self.graph.getMedianDegree()
    
    def updateHeapAndGraph(self, parsedLine):
        self.heapPush(parsedLine)
        self.graphAddNodes(parsedLine[1])
        
    def updateLatestTime(self, timeTag):
        self.latestTime=timeTag
        
    def balanceHeapAndGraph(self):
        while (self.latestTime-self.heap[0][0]) >= self.timeWindow:
            heapTop=self.heapPop() #pop up the heap top
            self.graphRemoveNodes(heapTop[1]) #remove the according nodes in the graph
            
    def heapPush(self, parsedLine):
        #define heap
        heapq.heappush(self.heap, parsedLine)
    
    def heapPop(self):
        return heapq.heappop(self.heap)

    def graphAddNodes(self, wordBag):
        self.graph.addNode(wordBag)
    
    def graphRemoveNodes(self, wordBag):
        self.graph.removeNode(wordBag)   

def median_degree(inFile, outFile):
    with open(inFile, 'r') as fin:
        with open(outFile,'w') as fout:
            runningWindow=RunningWindow()
            for line in fin:
                runningWindow.processLine(line)
                fout.write('%.2f\n' % (runningWindow.getMedianDegree()))

if __name__ == '__main__':
	median_degree(sys.argv[1], sys.argv[2])
