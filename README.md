IDE-CC Mar 2016
===========================================================

# Table of Contents
1. [Code Summary](README.md#Code-Summary)
2. [Parse the Json file](README.md#Parse-the-Json-file)
3. [Core data structure 1 store the latest time](README.md#Core-data-structure-1-store-the-latest-time)
4. [Core data structure 2 minheap](README.md#Core-data-structure-2-minheap)
5. [Core data structure 3 graph](README.md#Core-data-structure-3-graph)
6. [Misc 1 stream the input/output flow](README.md#Misc-1-stream-the-input/output-flow)
7. [Misc 2 itertools and decorator](README.md#Misc-2-itertools-and-decorator)
8. [Misc 3 min max heap and priority queue dictionary](README.md#Misc-3-MinMaxHeap-PriorityQueueDict)

In this coding challenge, I developed code in python to calculation the average degree of a running 60-second venmo transaction graph. In this code, modules json is used to parse the json format text, dateutil is used to parse the time strings to timestamps, heapq/sys are also used in the code. Please check whether these modules are in your python library. 

## Code Summary
[Back to Table of Contents](README.md#table-of-contents)

Firstly, I read in each line of the txt file. I transfer each line into a key/value pair. The key is the timetag translated from the "create_time” field. The value is the pair of words in the “target” and “actor” field. If the timetag (key) and the wordbag (value) are both valid and the value wordbag has both fields, this key/value pair is transferred to the next step. Otherwise nothing will be passed back.

Secondly, the valid timetag/wordbag (key/value) pair will be processed based on the timetag. If it is before in the 60-sec window, nothing will be done. Otherwise, the timetag/wordbag pair is inserted into a minheap, while the wordbag is inserted into a graph. And now if the timetag is after the recorded latest time, the recorded latest time is updated, and the minheap is rebalanced to pop up any timetag/wordbag pair that is now out of the 60-sec window, while the popped wordbag is also deleted from the graph.

Now comes the difficult part of the problem of using a data structure to easily maintain and calculate the rolling median. I use a combination of min heap and max heap to achieve this. This makes the median easy to find. Another thing that is important is I use a data structure of heap of dictionary in the base level (or priority queue dictionary). This data structure has the merits of O(1) access of both key and value, O(log(n)) insert/delete operation.

Finally, note that the number of edges and the number of nodes are updated accordingly within each heap/graph operation, one would only need to pull out their ratio to give the answer after each line of the txt file is processed.

## Parse the Json file
[Back to Table of Contents](README.md#table-of-contents)

The goal of this operation is to prepare a valid timetag/wordbag pair from the Json line for the next operation. 

First we need to use the json module to load the json text. The timetag is from the 'created_at' field and it has to be checked to see if it is there. Notice that the time string has the timezone '+xxxx' information so I used the dateutil module to automatically convert the string into integer seconds from Jan 01, 1970, UTC. 

Each json line has to have three fields. If all the conditions are satisfied, the valid wordbag is prepared for the next operation. Otherwise, nothin will be passed to the next step. I used the set data structure for the wordbag to avoid duplicates. 

## Core data structure 1 store the latest-time
[Back to Table of Contents](README.md#table-of-contents)

The lastest-time is an important variable needed by the minheap to determine which timetag/wordbag pairs are necessary for maintaining. It is one of the variables in the class named RunningWindow. It has a variable 'timeWindowInSeconds' to specify the span of the time window, whose default value in this code is 60 (sec). It has a varable 'averageDegree' to store the current graph degree value for output. It has a minheap and a graph.


## Core data structure 2 minheap
[Back to Table of Contents](README.md#table-of-contents)

The minheap maintains the timetag/wordbag pairs within the last 60 seconds. It also informs the graph which nodes/edges need to be added/deleted. The larger the timetag, the more latest into the future it is. So we just need to compare the timetag of the  minheap top with the current latest-time minus 60(sec). heapq module is used for the minheap here. The insert/delete operations of the minheap is both O(log(n)) on average, which makes it effecient on the run.


## Core data structure 3 graph
[Back to Table of Contents](README.md#table-of-contents)

I use dict data structure to represent the graph topology here. Each node, e.g. node A, is a key. All the connected nodes of node A is represented by another dict where the key is the node, the value (integer) is how many times that connection is represented from the wordbags in the current minheap. The integer value is used to determine whether a node/edge is new in the insertion operation, and whether a node/edge needs to be removed. Empty keys also needs to be removed. In each insert/delete operation, the total number of edges/nodes in the graph needs to be updated. This saves a lot of calculations in determining the average degree of the graph on the run.


## Misc 1 stream the input/output flow
[Back to Table of Contents](README.md#table-of-contents)

Only one line from the input file is read into the memory, processed by the parsing function and the functions in the RunningWindow class. When the average degree of the graph is determined, one record is passed to the file writing procedure. This streaming workflow saves a lot of memory on the run.


## Misc 2 decorator
[Back to Table of Contents](README.md#table-of-contents)

The decorator is used to simplify the similar part of the code and make it easier for me to modify.

## Misc 3 min max heap and priority queue dictionary
[Back to Table of Contents](README.md#table-of-contents)

I use a combination of min heap and max heap to achieve this. This makes the median easy to find. Another thing that is important is I use a data structure of heap of dictionary in the base level (or priority queue dictionary). This data structure has the merits of O(1) access of both key and value, O(log(n)) insert/delete operation.
